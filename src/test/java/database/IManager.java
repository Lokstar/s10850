package database;

import java.util.List;

import database.unitOfWork.EntityBase;

public interface IManager<T extends EntityBase> {

	public void save(T obj);
	public void delete(T obj);
	public T get(int id);
	public List<T> getAll();
}
