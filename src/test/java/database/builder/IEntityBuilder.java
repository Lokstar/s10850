package database.builder;
import java.sql.ResultSet;

import database.unitOfWork.EntityBase;

public interface IEntityBuilder<E extends EntityBase> {

        public E build(ResultSet rs);
        
}