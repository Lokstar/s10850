package database.builder;

import java.sql.ResultSet;
import model.Patient;

public class PatientBuilder implements IEntityBuilder<Patient>{

    public Patient build(ResultSet rs) {
            Patient result = null;
            try{      
                    result = new Patient();
                    result.setID(rs.getInt("id"));
                    result.setName(rs.getString("name"));
                    result.setSurname(rs.getString("surname"));

            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
