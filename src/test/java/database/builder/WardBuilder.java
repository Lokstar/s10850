package database.builder;

import java.sql.ResultSet;
import model.Ward;

public class WardBuilder implements IEntityBuilder<Ward>{

    public Ward build(ResultSet rs) {
            Ward result = null;
            try{      
                    result = new Ward();
                    result.setID(rs.getInt("id"));
                    result.setName(rs.getString("name"));

            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
