package database.builder;

import java.sql.ResultSet;
import model.Hospital;

public class HospitalBuilder implements IEntityBuilder<Hospital>{

    public Hospital build(ResultSet rs) {
            Hospital result = null;
            try{      
                    result = new Hospital();
                    result.setID(rs.getInt("id"));
                    result.setCity(rs.getString("city"));
                    result.setPostCode(rs.getString("postCode"));
                    result.setAdress(rs.getString("adress"));
                    result.setTelephonenumber(rs.getString("telephonenumber"));
                    
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
