package database.builder;

import java.sql.ResultSet;
import model.Doctor;

public class DoctorBuilder implements IEntityBuilder<Doctor>{

    public Doctor build(ResultSet rs) {
            Doctor result = null;
            try{      
                    result = new Doctor();
                    result.setID(rs.getInt("id"));
                    result.setName(rs.getString("name"));
                    result.setSurname(rs.getString("surname"));
                    result.setPesel(rs.getString("pesel"));
                    
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
