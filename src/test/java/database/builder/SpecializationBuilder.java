package database.builder;

import java.sql.ResultSet;
import model.Specialization;

public class SpecializationBuilder implements IEntityBuilder<Specialization>{

    public Specialization build(ResultSet rs) {
            Specialization result = null;
            try{      
                    result = new Specialization();
                    result.setID(rs.getInt("id"));
                    result.setName(rs.getString("name"));

            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
