package database.builder;

import java.sql.ResultSet;
import model.Card;

public class CardBuilder implements IEntityBuilder<Card>{

    public Card build(ResultSet rs) {
            Card result = null;
            try{      
                    result = new Card();
                    result.setID(rs.getInt("id"));
                    result.setDateOfIssue(rs.getString("dateOfIssue"));
                    result.setNumber(rs.getString("number"));
                    
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
