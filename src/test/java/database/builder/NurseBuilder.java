package database.builder;

import java.sql.ResultSet;
import model.Nurse;

public class NurseBuilder implements IEntityBuilder<Nurse>{

    public Nurse build(ResultSet rs) {
            Nurse result = null;
            try{      
                    result = new Nurse();
                    result.setID(rs.getInt("id"));
                    result.setName(rs.getString("name"));
                    result.setSurname(rs.getString("surname"));
                    result.setPesel(rs.getString("pesel"));
                    
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}
