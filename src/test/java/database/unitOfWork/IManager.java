package database.unitOfWork;
import java.util.List;

public interface IManager<T extends EntityBase> {

	public void save(T obj);
	public void delete(T obj);
	public T get(int id);
	public List<T> getAll();
}
