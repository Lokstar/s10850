package database.unitOfWork;

public abstract class EntityBase {

	public int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
