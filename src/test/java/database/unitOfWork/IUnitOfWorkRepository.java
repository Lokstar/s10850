package database.unitOfWork;


public interface IUnitOfWorkRepository {

	public void persistAdd(EntityBase ent);
	public void persistDeleted(EntityBase ent);
	public void persistUpdated(EntityBase ent);
	
}
