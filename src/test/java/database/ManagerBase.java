package database;

import java.util.List;

import database.unitOfWork.EntityBase;
import database.unitOfWork.IUnitOfWork;
import database.unitOfWork.IUnitOfWorkRepository;

public abstract class ManagerBase<E extends EntityBase> implements IManager<E>,IUnitOfWorkRepository {

	protected IUnitOfWork uow;
	

	public ManagerBase(IUnitOfWork uow) {
		super();
		this.uow = uow;
	}

	
	public void save(E obj) {
		uow.registerAdd(obj, this);
		
	}

	
	public void delete(E obj) {
		uow.registerDeleted(obj, this);
		
	}
	
	public final void SaveChanges()
	{
		uow.Commit();
	}
	
	
	public abstract E get(int id);

	
	public abstract List<E> getAll();
	
	
	public abstract void persistAdd(EntityBase ent);

	
	public abstract void persistDeleted(EntityBase ent);

	
	public abstract void persistUpdated(EntityBase ent);
	
}
