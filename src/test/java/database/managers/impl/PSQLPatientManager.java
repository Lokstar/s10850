package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Patient;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.IPatientManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLPatientManager extends ManagerBase<Patient> implements IPatientManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getPatientById;
	private PreparedStatement getAllPatient;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Patient> builder;
	private IPatientManager PatientMgr;
	
	private String createTablePatient = "CREATE TABLE Policies (ID serial, name varchar(33), surname varchar(33))";
    
	public PSQLPatientManager(IUnitOfWork uow, IEntityBuilder<Patient> builder,  IPatientManager PatientMgr) {
		super(uow);

		this.builder=builder;
        this.PatientMgr=PatientMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Patient).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Patient")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTablePatient);
			}
			
			getPatientById = connection.prepareStatement("SELECT * FROM Patient WHERE id=?");
			getAllPatient = connection.prepareStatement("SELECT * FROM Patient");
			insert = connection.prepareStatement("INSERT INTO Patient(name, surname) "
					+ "VALUES (?,?);");
			delete = connection.prepareStatement("DELETE FROM Patient WHERE id=?");
			update = connection.prepareStatement("UPDATE Patient SET name=? surname=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Patient get(int id) {
		Patient result = null;
		try{
			getPatientById.setInt(1, id);
			ResultSet rs = getPatientById.executeQuery();
			while(rs.next()){
				result = new Patient();
				result.setID(id);
				result.setName("Name");
				result.setSurname("Surname");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Patient> getAll() {
		List<Patient> Policies = new ArrayList<Patient>();
		try{
			ResultSet rs = getAllPatient.executeQuery();
			while(rs.next()){
				Patient result = new Patient();
				
				result = new Patient();
				result.setID(rs.getInt("id"));
				result.setName("name");
				result.setSurname("surname");
				
				Policies.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Policies;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Patient w = (Patient)ent;
		try{
			insert.setString(1, w.getName());
			insert.setString(2, w.getSurname());
			
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Patient w = (Patient)ent;
		try{
			delete.setInt(1, w.getID());
			
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Patient w = (Patient)ent;
		try{
			update.setString(1, w.getName());
			update.setString(2, w.getSurname());
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Patient obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Patient obj) {
		// TODO Auto-generated method stub
		
	}

}
