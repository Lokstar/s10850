package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Nurse;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.INurseManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLNurseManager extends ManagerBase<Nurse> implements INurseManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getNurseById;
	private PreparedStatement getAllNurses;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Nurse> builder;
	private INurseManager NurseMgr;
	
	private String createTableNurse = "CREATE TABLE Nurse (ID serial, name varchar(35), surname varchar(33), pesel varchar(15))";
			      
	public PSQLNurseManager(IUnitOfWork uow, IEntityBuilder<Nurse> builder,  INurseManager NurseMgr) {
		super(uow);

		this.builder=builder;
        this.NurseMgr=NurseMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Nurse).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Nurse")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableNurse);
				//System.out.println("Created.");
			}
			
			getNurseById = connection.prepareStatement("SELECT * FROM Nurse WHERE id=?");
			getAllNurses = connection.prepareStatement("SELECT * FROM Nurse");
			insert = connection.prepareStatement("INSERT INTO Nurse(Name, surname, pesel) "
					+ "VALUES (?,?,?);");
			delete = connection.prepareStatement("DELETE FROM Nurse WHERE id=?");
			update = connection.prepareStatement("UPDATE Nurse SET Name=? surname=? pesel=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Nurse get(int id) {
		Nurse result = null;
		try{
			getNurseById.setInt(1, id);
			ResultSet rs = getNurseById.executeQuery();
			while(rs.next()){
				result = new Nurse();
				result.setID(id);
				result.setName("Name");
				result.setSurname("Surname");
				result.setPesel("Pesel");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Nurse> getAll() {
		List<Nurse> InsuranceCompanies = new ArrayList<Nurse>();
		try{
			ResultSet rs = getAllNurses.executeQuery();
			while(rs.next()){
				Nurse result = new Nurse();
				
				result = new Nurse();
				result.setID(rs.getInt("id"));
				result.setName(rs.getString("Name"));
				result.setSurname(rs.getString("Surname"));
				result.setPesel(rs.getString("Pesel"));
				
				InsuranceCompanies.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return InsuranceCompanies;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Nurse w = (Nurse)ent;
		try{
			insert.setString(1, w.getName());
			insert.setString(2, w.getSurname());
			insert.setString(3, w.getPesel());
			
			
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Nurse w = (Nurse)ent;
		try{
			delete.setInt(1, w.getID());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Nurse w = (Nurse)ent;
		try{
			update.setString(1, w.getName());
			update.setString(2, w.getSurname());
			update.setString(3, w.getPesel());
			
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Nurse obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Nurse obj) {
		// TODO Auto-generated method stub
		
	}

}
