package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Doctor;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.IDoctorManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLDoctorManager extends ManagerBase<Doctor> implements IDoctorManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getDoctoresById;
	private PreparedStatement getAllDoctores;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Doctor> builder;
	private IDoctorManager DoctorMgr;
	
	private String createTableDoctor = "CREATE TABLE Doctores (ID serial, name varchar (30), surname varchar(60), pesel varchar(11))";
  
	public PSQLDoctorManager(IUnitOfWork uow, IEntityBuilder<Doctor> builder,  IDoctorManager DoctorMgr) {
		super(uow);

		this.builder=builder;
        this.DoctorMgr=DoctorMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Doctor).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Doctores")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableDoctor);
			}
			
			getDoctoresById = connection.prepareStatement("SELECT * FROM Doctores WHERE id=?");
			getAllDoctores = connection.prepareStatement("SELECT * FROM Doctores");
			insert = connection.prepareStatement("INSERT INTO Doctores(name, surname, pesel) "
					+ "VALUES (?,?,?);");
			delete = connection.prepareStatement("DELETE FROM Doctores WHERE id=?");
			update = connection.prepareStatement("UPDATE Doctores SET name=? surname=? pesel=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Doctor get(int id) {
		Doctor result = null;
		try{
			getDoctoresById.setInt(1, id);
			ResultSet rs = getDoctoresById.executeQuery();
			while(rs.next()){
				result = new Doctor();
				result.setID(id);
				result.setName("name");
				result.setSurname("surname");
				result.setPesel("pesel");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Doctor> getAll() {
		List<Doctor> Doctores = new ArrayList<Doctor>();
		try{
			ResultSet rs = getAllDoctores.executeQuery();
			while(rs.next()){
				Doctor result = new Doctor();
				
				result = new Doctor();
				result.setID(rs.getInt("id"));
				result.setName(rs.getString("name"));
				result.setSurname(rs.getString("surname"));
				result.setPesel(rs.getString("pesel"));
				
				Doctores.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Doctores;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Doctor w = (Doctor)ent;
		try{
			insert.setString(2, w.getName());
			insert.setString(1, w.getSurname());
			insert.setString(3, w.getPesel());
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Doctor w = (Doctor)ent;
		try{
			delete.setInt(1, w.getID());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Doctor w = (Doctor)ent;
		try{
			update.setString(2, w.getName());
			update.setString(1, w.getSurname());
			update.setString(3, w.getPesel());
			
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Doctor obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Doctor obj) {
		// TODO Auto-generated method stub
		
	}

}
