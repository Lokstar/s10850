package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Specialization;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.ISpecializationManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLSpecializationManager extends ManagerBase<Specialization> implements ISpecializationManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getSpecializationsById;
	private PreparedStatement getAllSpecializations;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Specialization> builder;
	private ISpecializationManager SpecializationMgr;
	
	private String createTableSpecialization = "CREATE TABLE Specializations (ID serial, Name varchar(33))";
    
	 
		public PSQLSpecializationManager(IUnitOfWork uow, IEntityBuilder<Specialization> builder,  ISpecializationManager SpecializationMgr) {
			super(uow);

			this.builder=builder;
	        this.SpecializationMgr=SpecializationMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Specialization).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Specializations")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableSpecialization);
			}
			
			getSpecializationsById = connection.prepareStatement("SELECT * FROM Specializations WHERE id=?");
			getAllSpecializations = connection.prepareStatement("SELECT * FROM Specializations");
			insert = connection.prepareStatement("INSERT INTO Specializations(Name) "
					+ "VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM Specializations WHERE id=?");
			update = connection.prepareStatement("UPDATE Specializations SET Name=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Specialization get(int id) {
		Specialization result = null;
		try{
			getSpecializationsById.setInt(1, id);
			ResultSet rs = getSpecializationsById.executeQuery();
			while(rs.next()){
				result = new Specialization();
				result.setID(id);
				result.setName("Specialization");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Specialization> getAll() {
		List<Specialization> Specializations = new ArrayList<Specialization>();
		try{
			ResultSet rs = getAllSpecializations.executeQuery();
			while(rs.next()){
				Specialization result = new Specialization();
				
				result = new Specialization();
				result.setID(rs.getInt("id"));
				result.setName("Name");
				
				Specializations.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Specializations;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Specialization w = (Specialization)ent;
		try{
			insert.setString(1, w.getName());
			
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Specialization w = (Specialization)ent;
		try{
			delete.setInt(1, w.getID());
			
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Specialization w = (Specialization)ent;
		try{
			update.setString(1, w.getName());
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Specialization obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Specialization obj) {
		// TODO Auto-generated method stub
		
	}

}
