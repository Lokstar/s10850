package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Card;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.ICardManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLCardManager extends ManagerBase<Card> implements ICardManager{
	private IEntityBuilder<Card> builder;
	private ICardManager CardMgr;
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getCardsById;
	private PreparedStatement getAllCards;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	
	
	private String createTableCards = "CREATE TABLE Cards (ID serial, dateOfIssue varchar(25), number varchar(40));";
    
	public PSQLCardManager(IUnitOfWork uow, IEntityBuilder<Card> builder,  ICardManager CardMgr) {
		super(uow);

		this.builder=builder;
        this.CardMgr=CardMgr;
        
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Card).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Cards")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableCards);
			}
			
			getCardsById = connection.prepareStatement("SELECT * FROM Cards WHERE id=?");
			getAllCards = connection.prepareStatement("SELECT * FROM Cards");
			insert = connection.prepareStatement("INSERT INTO Cards(dateOfIssue,number) "
					+ "VALUES (?,?);");
			delete = connection.prepareStatement("DELETE FROM Cards WHERE id=?");
			update = connection.prepareStatement("UPDATE Cards SET ID =? dateOfIssue=? number=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Card get(int id) {
		Card result = null;
		try{
			getCardsById.setInt(1, id);
			ResultSet rs = getCardsById.executeQuery();
			while(rs.next()){
				result = new Card();
				result.setID(id);
				result.setDateOfIssue("dateOfIssue");
				result.setNumber("number");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Card> getAll() {
		List<Card> Cards = new ArrayList<Card>();
		try{
			ResultSet rs = getAllCards.executeQuery();
			while(rs.next()){
				Card result = new Card();
				
				result = new Card();
				result.setID(rs.getInt("id"));
				result.setDateOfIssue(rs.getString("dateOfIssue"));
				result.setNumber(rs.getString("number"));
				
				
				Cards.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Cards;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Card w = (Card)ent;
		try{
			insert.setString(1, w.getDateOfIssue());
			insert.setString(2, w.getNumber());
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Card w = (Card)ent;
		try{
			delete.setInt(1, w.getID());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Card w = (Card)ent;
		try{
			update.setInt(1, w.getID());
			update.setString(2, w.getDateOfIssue());
			update.setString(3, w.getNumber());
			
			
			update.executeUpdate();
		
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Card obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Card obj) {
		// TODO Auto-generated method stub
		
	}

}
