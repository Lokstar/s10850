package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.Ward;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.IWardManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLWardManager extends ManagerBase<Ward> implements IWardManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getWardsById;
	private PreparedStatement getAllWards;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Ward> builder;
	private IWardManager WardMgr;
	
	private String createTableWard = "CREATE TABLE Wards (ID serial, Name varchar(33))";
    
	 
		public PSQLWardManager(IUnitOfWork uow, IEntityBuilder<Ward> builder,  IWardManager WardMgr) {
			super(uow);

			this.builder=builder;
	        this.WardMgr=WardMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Ward).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Wards")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableWard);
			}
			
			getWardsById = connection.prepareStatement("SELECT * FROM Wards WHERE id=?");
			getAllWards = connection.prepareStatement("SELECT * FROM Wards");
			insert = connection.prepareStatement("INSERT INTO Wards(Name) "
					+ "VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM Wards WHERE id=?");
			update = connection.prepareStatement("UPDATE Wards SET Name=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Ward get(int id) {
		Ward result = null;
		try{
			getWardsById.setInt(1, id);
			ResultSet rs = getWardsById.executeQuery();
			while(rs.next()){
				result = new Ward();
				result.setID(id);
				result.setName("Ward");
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Ward> getAll() {
		List<Ward> Wards = new ArrayList<Ward>();
		try{
			ResultSet rs = getAllWards.executeQuery();
			while(rs.next()){
				Ward result = new Ward();
				
				result = new Ward();
				result.setID(rs.getInt("id"));
				result.setName("Name");
				
				Wards.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Wards;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Ward w = (Ward)ent;
		try{
			insert.setString(1, w.getName());
			
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Ward w = (Ward)ent;
		try{
			delete.setInt(1, w.getID());
			
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Ward w = (Ward)ent;
		try{
			update.setString(1, w.getName());
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Ward obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Ward obj) {
		// TODO Auto-generated method stub
		
	}

}
