package database.managers.impl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import model.Hospital;
import database.unitOfWork.EntityBase;
import database.builder.IEntityBuilder;
import database.managers.IHospitalManager;
import database.ManagerBase;
import database.unitOfWork.IUnitOfWork;

public class PSQLHospitalManager extends ManagerBase<Hospital> implements IHospitalManager{
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getHospitalsById;
	private PreparedStatement getAllHospitals;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Hospital> builder;
	private IHospitalManager HospitalMgr;
	
	private String createTableHospital = "CREATE TABLE Hospitals (ID serial, city(25), postCode(40), adress(40),telephonenumber(60));";
    
	public PSQLHospitalManager(IUnitOfWork uow, IEntityBuilder<Hospital> builder,  IHospitalManager HospitalMgr) {
		super(uow);

		this.builder=builder;
        this.HospitalMgr=HospitalMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			System.out.println("Logged (Hospital).");
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				String TableName = rs.getString(2);
				if(TableName.equalsIgnoreCase("Hospitals")){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableHospital);
			}
			
			getHospitalsById = connection.prepareStatement("SELECT * FROM Hospitals WHERE id=?");
			getAllHospitals = connection.prepareStatement("SELECT * FROM Hospitals");
			insert = connection.prepareStatement("INSERT INTO Hospitals(city,postcode,address,telephonenumber) "
					+ "VALUES (?,?,?,?);");
			delete = connection.prepareStatement("DELETE FROM Hospitals WHERE id=?");
			update = connection.prepareStatement("UPDATE Hospitals SET city=? postcode=? adress=? telephonenumber=? WHERE id=?");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	@Override
	public Hospital get(int id) {
		Hospital result = null;
		try{
			getHospitalsById.setInt(1, id);
			ResultSet rs = getHospitalsById.executeQuery();
			while(rs.next()){
				result = new Hospital();
				result.setID(id);
				result.setCity("city");
				result.setPostCode("postCode");
				result.setAdress("address");
				result.setTelephonenumber("telephonenumber");
				
				
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Hospital> getAll() {
		List<Hospital> Hospitals = new ArrayList<Hospital>();
		try{
			ResultSet rs = getAllHospitals.executeQuery();
			while(rs.next()){
				Hospital result = new Hospital();
				
				result = new Hospital();
				result.setID(rs.getInt("id"));
				result.setCity(rs.getString("City"));
				result.setPostCode(rs.getString("postCode"));
				result.setAdress(rs.getString("Adress"));
				result.setTelephonenumber(rs.getString("telephonenumber"));
				
				
				Hospitals.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return Hospitals;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Hospital w = (Hospital)ent;
		try{
			insert.setString(1, w.getCity());
			insert.setString(2, w.getPostCode());
			insert.setString(3, w.getAdress());
			insert.setString(4, w.getTelephonenumber());
			insert.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Hospital w = (Hospital)ent;
		try{
			delete.setInt(1, w.getID());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Hospital w = (Hospital)ent;
		try{
			update.setString(1, w.getCity());
			update.setString(2, w.getPostCode());
			update.setString(3, w.getAdress());
			update.setString(4, w.getTelephonenumber());
			update.setInt(5, w.getID());
			update.executeUpdate();

			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}


	public void save(Hospital obj) {
		// TODO Auto-generated method stub
		
	}


	public void delete(Hospital obj) {
		// TODO Auto-generated method stub
		
	}

}
