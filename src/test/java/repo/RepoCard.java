package repo;

import java.util.List;

import model.Card;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoCard implements IRepository<Card>{


	MockDb db;
	public RepoCard(MockDb db){
		this.db=db;
	}
	
	public Card get(int id) {
		return (Card)db.get(id);
	}

	public List<Card> getAll() {
		return db.getItemsByType(Card.class);
	}

	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoCard [db=" + db + "]";
	}
	
}
