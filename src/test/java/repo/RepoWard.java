package repo;

import java.util.List;

import model.Ward;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoWard implements IRepository<Ward>{


	MockDb db;
	public RepoWard(MockDb db){
		this.db=db;
	}
	
	public Ward get(int id) {
		return (Ward)db.get(id);
	}

	public List<Ward> getAll() {
		return db.getItemsByType(Ward.class);
	}

	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoCard [db=" + db + "]";
	}
	
}
