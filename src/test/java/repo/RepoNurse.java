package repo;


import java.util.List;

import model.Nurse;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoNurse implements IRepository<Nurse>{


	MockDb db;
	public RepoNurse(MockDb db){
		this.db=db;
	}
	
	public Nurse get(int id) {
		return (Nurse)db.get(id);
	}

	
	public List<Nurse> getAll() {
		return db.getItemsByType(Nurse.class);
	}

	
	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
