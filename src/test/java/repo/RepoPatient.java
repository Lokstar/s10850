package repo;


import java.util.List;

import model.Patient;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoPatient implements IRepository<Patient>{


	MockDb db;
	public RepoPatient(MockDb db){
		this.db=db;
	}
	
	public Patient get(int id) {
		return (Patient)db.get(id);
	}

	
	public List<Patient> getAll() {
		return db.getItemsByType(Patient.class);
	}

	
	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
