package repo;


import java.util.List;

import model.Hospital;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoHospital implements IRepository<Hospital>{


	MockDb db;
	public RepoHospital(MockDb db){
		this.db=db;
	}
	
	public Hospital get(int id) {
		return (Hospital)db.get(id);
	}

	
	public List<Hospital> getAll() {
		return db.getItemsByType(Hospital.class);
	}

	
	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
