package repo;


import java.util.List;

import model.Doctor;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoDoctor implements IRepository<Doctor>{


	MockDb db;
	public RepoDoctor(MockDb db){
		this.db=db;
	}
	
	public Doctor get(int id) {
		return (Doctor)db.get(id);
	}

	
	public List<Doctor> getAll() {
		return db.getItemsByType(Doctor.class);
	}

	
	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
