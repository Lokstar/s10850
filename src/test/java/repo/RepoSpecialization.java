package repo;


import java.util.List;

import model.Specialization ;
import database.MockDb;
import database.unitOfWork.EntityBase;

public class RepoSpecialization implements IRepository<Specialization>{


	MockDb db;
	public RepoSpecialization(MockDb db){
		this.db=db;
	}
	
	public Specialization get(int id) {
		return (Specialization)db.get(id);
	}

	
	public List<Specialization> getAll() {
		return db.getItemsByType(Specialization.class);
	}

	
	public void save(EntityBase E) {
		db.save(E);
	}

	
	public void delete(EntityBase E) {
		db.delete(E);
	}

	
	public void update(EntityBase E) {
		
	}
	
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
