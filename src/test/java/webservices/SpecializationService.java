package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Specialization;
import database.builder.SpecializationBuilder;
import database.builder.IEntityBuilder;
import database.managers.ISpecializationManager;
import database.managers.impl.PSQLSpecializationManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class SpecializationService {

		@WebMethod
        public SpecializationDto giveBook(SpecializationDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Specialization> builder = new SpecializationBuilder();
                ISpecializationManager mgr = new PSQLSpecializationManager(uow, builder, null);
                Specialization w = mgr.get(request.getID());
                SpecializationDto result = new SpecializationDto();
                result.setName(w.getName());
                result.setID(w.getID());
                
                return result;
                
        }
        
        @WebMethod
        public SpecializationDto saveBook(SpecializationDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Specialization> builder = new SpecializationBuilder();
                ISpecializationManager mgr = new PSQLSpecializationManager(uow, builder, null);
                Specialization w = new Specialization();
                w.setName(w.getName());
                w.setID(w.getID());
                
                mgr.save(w);
                uow.Commit();
                SpecializationDto result = new SpecializationDto();
                result.setName(w.getName());
                result.setID(w.getID());
                
                return result;
                
        }
}
