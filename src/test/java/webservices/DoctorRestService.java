package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Doctor;
import database.managers.IDoctorManager;
import dto.DoctorDto;


@Path("Doctor")
@Stateless
public class DoctorRestService {
        
        private IDoctorManager mgr;
        
        @GET
        @Path("/giveDoctor/{id}")
        @Produces("application/json")
        public DoctorDto giveDoctor(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Doctor> builder = new DoctorBuilder();
                //IDoctorManager mgr = new PSQLDoctorManager(uow,builder,null);
                Doctor a = mgr.get(id);
                
                DoctorDto result = new DoctorDto();
                result.setID(id);
				result.setName(a.getName());
				result.setSurname(a.getSurname());
				result.setPesel(a.getPesel());
                return result;
                
        }
        
        public DoctorDto saveDoctor(DoctorDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Doctor> builder = new DoctorBuilder();
                //IDoctorManager mgr = new PSQLDoctorManager(uow,builder,null);
                Doctor a = new Doctor();

                a.setName(request.getName());
                a.setSurname(request.getSurname());
                a.setPesel(request.getPesel());
                
                mgr.save(a);
                //uow.Commit();
                DoctorDto result = new DoctorDto();
                result.setName(a.getName());
                result.setSurname(a.getSurname());
                result.setPesel(a.getPesel());
                
                return result;
                
        }
}