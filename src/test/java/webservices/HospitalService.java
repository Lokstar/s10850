package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Hospital;
import database.builder.HospitalBuilder;
import database.builder.IEntityBuilder;
import database.managers.IHospitalManager;
import database.managers.impl.PSQLHospitalManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class HospitalService {

		@WebMethod
        public HospitalDto giveBook(HospitalDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Hospital> builder = new HospitalBuilder();
                IHospitalManager mgr = new PSQLHospitalManager(uow, builder, null);
                Hospital w = mgr.get(request.getID());
                HospitalDto result = new HospitalDto();
                
            	result.setCity(w.getCity());
				result.setPostCode(w.getPostCode());
				result.setAdress(w.getAdress());
				result.setTelephonenumber(w.getTelephonenumber());
                return result;
                
        }
        
        @WebMethod
        public HospitalDto saveBook(HospitalDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Hospital> builder = new HospitalBuilder();
                IHospitalManager mgr = new PSQLHospitalManager(uow, builder, null);
                Hospital w = new Hospital();
                w.setCity(w.getCity());
                w.setPostCode(w.getPostCode());
                w.setAdress(w.getAdress());
                w.setTelephonenumber(w.getTelephonenumber());
                mgr.save(w);
                uow.Commit();
                HospitalDto result = new HospitalDto();
                
                result.setCity(w.getCity());
				result.setPostCode(w.getPostCode());
				result.setAdress(w.getAdress());
				result.setTelephonenumber(w.getTelephonenumber());
                return result;
                
        }
}
