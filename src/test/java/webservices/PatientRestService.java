package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Patient;
import database.managers.IPatientManager;
import dto.PatientDto;


@Path("Patient")
@Stateless
public class PatientRestService {
        
        private IPatientManager mgr;
        
        @GET
        @Path("/givePatient/{id}")
        @Produces("application/json")
        public PatientDto givePatient(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Patient> builder = new PatientBuilder();
                //IPatientManager mgr = new PSQLPatientManager(uow,builder,null);
                Patient a = mgr.get(id);
                
                PatientDto result = new PatientDto();
                result.setID(id);
				result.setName(a.getName());
				result.setSurname(a.getSurname());				
                return result;
                
        }
        
        public PatientDto savePatient(PatientDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Patient> builder = new PatientBuilder();
                //IPatientManager mgr = new PSQLPatientManager(uow,builder,null);
                Patient a = new Patient();

                a.setID(request.getID());
                a.setName(request.getName());
                a.setSurname(request.getSurname());
                
                mgr.save(a);
                //uow.Commit();
                PatientDto result = new PatientDto();
                result.setID(a.getID());
                result.setName(a.getName());
                result.setSurname(a.getSurname());
                
                return result;
                
        }
}