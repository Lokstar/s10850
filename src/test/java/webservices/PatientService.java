package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Patient;
import database.builder.PatientBuilder;
import database.builder.IEntityBuilder;
import database.managers.IPatientManager;
import database.managers.impl.PSQLPatientManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class PatientService {

		@WebMethod
        public PatientDto giveBook(PatientDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Patient> builder = new PatientBuilder();
                IPatientManager mgr = new PSQLPatientManager(uow, builder, null);
                Patient w = mgr.get(request.getID());
                PatientDto result = new PatientDto();
                result.setName(w.getName());
                result.setSurname(w.getSurname());
                result.setID(w.getID());
                
                return result;
                
        }
        
        @WebMethod
        public PatientDto saveBook(PatientDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Patient> builder = new PatientBuilder();
                IPatientManager mgr = new PSQLPatientManager(uow, builder, null);
                Patient w = new Patient();
                w.setName(w.getName());
                w.setSurname(w.getSurname());
                
                w.setID(w.getID());
                
                
                mgr.save(w);
                uow.Commit();
                PatientDto result = new PatientDto();
                result.setName(w.getName());
                result.setSurname(w.getSurname());
                
                result.setID(w.getID());
                
                return result;
                
        }
}
