package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Card;
import database.managers.ICardManager;
import dto.CardDto;


@Path("Card")
@Stateless
public class CardRestService {
        
       
        private ICardManager mgr;
        
        @GET
        @Path("/givecard/{id}")
        @Produces("application/json")
        public CardDto giveCard(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Agent> builder = new AgentBuilder();
                //IAgentManager mgr = new PSQLAgentManager(uow,builder,null);
                Card a = mgr.get(id);
                
                CardDto result = new CardDto();
                result.setId(id);
				result.setdateOfIssue(a.getDateOfIssue());
				result.setnumber(a.getNumber());
				
                return result;
                
        }
        
        public CardDto saveCard(CardDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Agent> builder = new AgentBuilder();
                //IAgentManager mgr = new PSQLAgentManager(uow,builder,null);
                Card a = new Card();

                
                
                
                
                mgr.save(a);
                //uow.Commit();
                CardDto result = new CardDto();
                result.setdateOfIssue(a.getDateOfIssue());
                result.setnumber(a.getNumber());
                
                
                return result;
                
        }
}