package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Nurse;
import database.managers.INurseManager;
import dto.NurseDto;


@Path("Nurse")
@Stateless
public class NurseRestService {
        
        private INurseManager mgr;
        
        @GET
        @Path("/giveNurse/{id}")
        @Produces("application/json")
        public NurseDto giveNurse(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Nurse> builder = new NurseBuilder();
                //INurseManager mgr = new PSQLNurseManager(uow,builder,null);
                Nurse a = mgr.get(id);
                
                NurseDto result = new NurseDto();
                result.setID(id);
				result.setName(a.getName());
				result.setSurname(a.getSurname());
				result.setPesel(a.getPesel());
				
                return result;
                
        }
        
        public NurseDto saveNurse(NurseDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Nurse> builder = new NurseBuilder();
                //INurseManager mgr = new PSQLNurseManager(uow,builder,null);
                Nurse a = new Nurse();

                a.setName(request.getName());
                a.setSurname(request.getSurname());
                a.setPesel(request.getPesel());
                
                mgr.save(a);
                //uow.Commit();
                NurseDto result = new NurseDto();
                result.setName(a.getName());
                result.setSurname(a.getSurname());
                result.setPesel(a.getPesel());
                
                
                
                return result;
                
        }
}