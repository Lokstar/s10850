package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Card;
import database.builder.CardBuilder;
import database.builder.IEntityBuilder;
import database.managers.ICardManager;
import database.managers.impl.PSQLCardManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class CardService {

		@WebMethod
        public CardDto giveBook(CardDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Card> builder = new CardBuilder();
                ICardManager mgr = new PSQLCardManager(uow, builder, null);
                Card w = mgr.get(request.getId());
                CardDto result = new CardDto();
                result.setdateOfIssue(w.getDateOfIssue());
                result.setnumber(w.getNumber());
                
                return result;
                
        }
        
        @WebMethod
        public CardDto saveBook(CardDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Card> builder = new CardBuilder();
                ICardManager mgr = new PSQLCardManager(uow, builder, null);
                Card w = new Card();
                w.setDateOfIssue(w.getDateOfIssue());
                w.setNumber(w.getNumber());
               
                mgr.save(w);
                uow.Commit();
                CardDto result = new CardDto();
                result.setdateOfIssue(w.getDateOfIssue());
                result.setnumber(w.getNumber());
                return result;
                
        }
}
