package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Nurse;
import database.builder.NurseBuilder;
import database.builder.IEntityBuilder;
import database.managers.INurseManager;
import database.managers.impl.PSQLNurseManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class NurseService {

		@WebMethod
        public NurseDto giveBook(NurseDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Nurse> builder = new NurseBuilder();
                INurseManager mgr = new PSQLNurseManager(uow, builder, null);
                Nurse w = mgr.get(request.getID());
                
                NurseDto result = new NurseDto();
                result.setName(w.getName());
                
                return result;
                
        }
        
        @WebMethod
        public NurseDto saveBook(NurseDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Nurse> builder = new NurseBuilder();
                INurseManager mgr = new PSQLNurseManager(uow, builder, null);
                Nurse w = new Nurse();
                w.setName(w.getName());
                w.setSurname(w.getSurname());
                w.setPesel(w.getPesel());
                
               
                
                mgr.save(w);
                uow.Commit();
                NurseDto result = new NurseDto();
                result.setName(w.getName());
                result.setSurname(w.getSurname());
                result.setPesel(w.getPesel());
                
                
                return result;
                
        }
}
