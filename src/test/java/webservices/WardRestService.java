package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Ward;
import database.managers.IWardManager;
import dto.WardDto;


@Path("Ward")
@Stateless
public class WardRestService {
        
        private IWardManager mgr;
        
        @GET
        @Path("/giveWard/{id}")
        @Produces("application/json")
        public WardDto giveWard(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Ward> builder = new WardBuilder();
                //IWardManager mgr = new PSQLWardManager(uow,builder,null);
                Ward a = mgr.get(id);
                
                WardDto result = new WardDto();
                result.setID(id);
				result.setName(a.getName());
                return result;
                
        }
        
        public WardDto saveWard(WardDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Ward> builder = new WardBuilder();
                //IWardManager mgr = new PSQLWardManager(uow,builder,null);
                Ward a = new Ward();

                a.setName(request.getName());

                mgr.save(a);
                //uow.Commit();
                WardDto result = new WardDto();
                result.setName(a.getName());
                
                return result;
                
        }
}