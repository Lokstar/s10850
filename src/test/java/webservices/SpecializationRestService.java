package webservices;

import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Specialization;
import database.managers.ISpecializationManager;
import dto.SpecializationDto;


@Path("Specialization")
@Stateless
public class SpecializationRestService {
        
        private ISpecializationManager mgr;
        
        @GET
        @Path("/giveSpecialization/{id}")
        @Produces("application/json")
        public SpecializationDto giveSpecialization(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Specialization> builder = new SpecializationBuilder();
                //ISpecializationManager mgr = new PSQLSpecializationManager(uow,builder,null);
                Specialization a = mgr.get(id);
                
                SpecializationDto result = new SpecializationDto();
                result.setID(id);
				result.setName(a.getName());
                return result;
                
        }
        
        public SpecializationDto saveSpecialization(SpecializationDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Specialization> builder = new SpecializationBuilder();
                //ISpecializationManager mgr = new PSQLSpecializationManager(uow,builder,null);
                Specialization a = new Specialization();

                a.setName(request.getName());

                mgr.save(a);
                //uow.Commit();
                SpecializationDto result = new SpecializationDto();
                result.setName(a.getName());
                
                return result;
                
        }
}