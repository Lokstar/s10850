package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Ward;
import database.builder.WardBuilder;
import database.builder.IEntityBuilder;
import database.managers.IWardManager;
import database.managers.impl.PSQLWardManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class WardService {

		@WebMethod
        public WardDto giveBook(WardDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Ward> builder = new WardBuilder();
                IWardManager mgr = new PSQLWardManager(uow, builder, null);
                Ward w = mgr.get(request.getID());
                WardDto result = new WardDto();
                result.setName(w.getName());
                result.setID(w.getID());
                
                return result;
                
        }
        
        @WebMethod
        public WardDto saveBook(WardDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Ward> builder = new WardBuilder();
                IWardManager mgr = new PSQLWardManager(uow, builder, null);
                Ward w = new Ward();
                w.setName(w.getName());
                w.setID(w.getID());
                
                mgr.save(w);
                uow.Commit();
                WardDto result = new WardDto();
                result.setName(w.getName());
                result.setID(w.getID());
                
                return result;
                
        }
}
