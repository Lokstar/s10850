package webservices;
import javax.ejb.Stateless;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Hospital;
import database.managers.IHospitalManager;
import dto.HospitalDto;

@Path("Hospital")
@Stateless
public class HospitalRestService {
        
        private IHospitalManager mgr;
        
        @GET
        @Path("/giveHospital/{id}")
        @Produces("application/json")
        public HospitalDto giveHospital(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Hospital> builder = new HospitalBuilder();
                //IHospitalManager mgr = new PSQLHospitalManager(uow,builder,null);
                Hospital a = mgr.get(id);
                
                HospitalDto result = new HospitalDto();
                result.setID(id);
				result.setCity(a.getCity());
				result.setPostCode(a.getPostCode());
				result.setAdress(a.getAdress());
				result.setTelephonenumber(a.getTelephonenumber());
				
                return result;
                
        }
        
        public HospitalDto saveHospital(HospitalDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Hospital> builder = new HospitalBuilder();
                //IHospitalManager mgr = new PSQLHospitalManager(uow,builder,null);
                Hospital a = new Hospital();

                a.setCity(request.getCity());
                a.setPostCode(request.getPostCode());
                a.setAdress(request.getAdress());
                a.setTelephonenumber(request.getTelephonenumber());
                
                
                mgr.save(a);
                //uow.Commit();
                HospitalDto result = new HospitalDto();
               
				result.setCity(a.getCity());
				result.setPostCode(a.getPostCode());
				result.setAdress(a.getAdress());
				result.setTelephonenumber(a.getTelephonenumber());
                
                return result;
                
        }
}