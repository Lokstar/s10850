package webservices;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Doctor;
import database.builder.DoctorBuilder;
import database.builder.IEntityBuilder;
import database.managers.IDoctorManager;
import database.managers.impl.PSQLDoctorManager;
import database.unitOfWork.UnitOfWork;
import dto.*;

@WebService
public class DoctorService {

		@WebMethod
        public DoctorDto giveBook(DoctorDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Doctor> builder = new DoctorBuilder();
                IDoctorManager mgr = new PSQLDoctorManager(uow, builder, null);
                Doctor w = mgr.get(request.getID());
                DoctorDto result = new DoctorDto();
                result.setName(w.getName());
                result.setSurname(w.getSurname());
                result.setPesel(w.getPesel());
                
                return result;
                
        }
        
        @WebMethod
        public DoctorDto saveBook(DoctorDto request) throws InstantiationException, IllegalAccessException, ClassNotFoundException
        {
                UnitOfWork uow = new UnitOfWork();
                IEntityBuilder<Doctor> builder = new DoctorBuilder();
                IDoctorManager mgr = new PSQLDoctorManager(uow, builder, null);
                Doctor w = new Doctor();
                w.setName(w.getName());
                w.setSurname(w.getSurname());
                w.setPesel(w.getPesel());
                
                mgr.save(w);
                uow.Commit();
                DoctorDto result = new DoctorDto();
                result.setName(w.getName());
                result.setSurname(w.getSurname());
                result.setPesel(w.getPesel());
                return result;
                
        }
}
