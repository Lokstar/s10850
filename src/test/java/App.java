

/**
 * Hello world!
 *
 */
import java.sql.*;
public class App 
{
    public static void main( String[] args )
    {
    	String url = "jdbc:postgresql://localhost:5432/postgres";
    	try {
    		Connection connection = DriverManager.getConnection(url, "postgres", "123");
    		System.out.println("Pomy�lnie po��czono z baz�...");

    		Statement statement = connection.createStatement();

    		try {
    			statement.executeUpdate("create table card(id int, dateOfIssue varchar(64), number varchar(32));");
    		}
    		catch(SQLException e) {}
    		try {
    			statement.executeUpdate("create table doctor(id int, name varchar(16), surname varchar(32), pesel varchar(11));");
    		}
    		catch(SQLException e) {}
    		try {
    			statement.executeUpdate("create table hospital(id int, city varchar(16), postCode varchar(6), adress varchar(32), telephonenumber varchar(64));");
    		}
    		catch(SQLException e) {}
    		try {
    			statement.executeUpdate("create table nurse(id int, name varchar(16), surname varchar(32), pesel varchar(11));");
    		}
    		catch(SQLException e) {}
    		try {
    			statement.executeUpdate("create table patient(id int, name varchar(16), surname varchar(32));");
    		}
    		catch(SQLException e) {}
    		
    		try {
    			statement.executeUpdate("create table specialization(id int, name varchar(16));");
    		}
    		catch(SQLException e) {}
    		
    		try {
    			statement.executeUpdate("create table ward(id int, name varchar(16));");
    		}
    		catch(SQLException e) {}

    	
    	}
    	catch(SQLException e) {
    		e.printStackTrace();
    		
    	}
    	System.out.println("Pomy�lnie po��czono z baz� ... tablice utworzone");
    }
}
