package dto;

public class CardDto {
	
	public int ID;
	public String dateOfIssue;
	public String number;
	
	
	public int getId() {
		return ID;
	}

	public void setId(int id) {
		ID = id;
	}

	public String getdateOfIssue() {
		return dateOfIssue;
	}

	public void setdateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getnumber() {
		return number;
	}

	public void setnumber(String number) {
		this.number = number;
	}

	
	}

